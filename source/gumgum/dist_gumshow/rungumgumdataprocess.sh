#!/bin/bash
export SPARK_MAJOR_VERSION=2
/usr/bin/nohup spark-submit \
--class com.att.datasolutions.GumgumDataProcess \
--conf "spark.driver.extraJavaOptions=-Dconfig.file=gumgum.conf" \
--conf "spark.driver.extraJavaOptions -Dderby.stream.error.file=/home/dist_gumshow/logs" \
--conf "spark.yarn.executor.memoryOverhead=4000" \
--conf "spark.yarn.driver.memoryOverhead=4000" \
--conf "spark.executor.extraJavaOptions=-XX:+UseG1GC -verbose:gc -XX:+PrintGCDetails" \
--conf "spark.task.maxFailures=10" \
--conf "spark.sql.shuffle.partitions=2000" \
--conf spark.driver.maxResultSize=3g \
--files /home/dist_gumshow/gumgum.conf \
--master yarn \
--deploy-mode cluster \
--num-executors 40 \
--executor-cores 4 \
--executor-memory 40G \
--driver-memory 60G \
--queue=common_user \
gum_gum-assembly-1.0.jar > ./spark-output.log 2>./spark-error.log &