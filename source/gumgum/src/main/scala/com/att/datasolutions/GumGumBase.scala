package com.att.datasolutions


import com.typesafe.config.ConfigFactory
import java.util.logging.Logger
import scala.util.Try
import java.util.logging.Level
import org.apache.spark.sql.ColumnName
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.SparkSession

object GumGumBase {
  protected val configName = "gumgum"
  val config = ConfigFactory.load
  val processRunMode = "gumgum.common.processtestmode"
  val configLogger = Logger.getLogger(this.getClass.getName)

  val sparkloglevel = Try(config.getString("gumgum.common.sparkloglevel")).getOrElse("WARN")
  val log = Logger.getLogger(this.getClass.getName)
  /*
  org.apache.log4j.Logger.getLogger("org.apache.spark").setLevel(org.apache.log4j.Level.WARN)
  org.apache.log4j.Logger.getLogger("org.apache.hadoop").setLevel(org.apache.log4j.Level.WARN)
  * 
  */
  val spark = {
    if (Try(config.getBoolean(processRunMode)).getOrElse(false)) {
      log.info("GumgumDataProcess running in TEST mode")
      SparkSession
        .builder
        .enableHiveSupport
        .master("local[4]")
        .appName("GumgumDataProcess")
        .getOrCreate
    } else {
      SparkSession
        .builder
        .enableHiveSupport
        .appName("GumgumDataProcess")
        .getOrCreate
    }
  }
  spark.conf.set("spark.sql.orc.filterPushdown", "true")
  spark.conf.set("spark.sql.hive.convertMetastoreOrc", "false")
  spark.sparkContext.setLogLevel(sparkloglevel)
  val configAppValue = configName + "." + "gumgumdataprocess"

  spark.sparkContext.setLogLevel(sparkloglevel)
  configLogger.setLevel(Level.INFO)

  import scala.collection.JavaConverters._

  //val dir = config.getString(configAppValue + "." + "input_dir")
  val input_dir: List[String] = {
    config.getStringList(
      configAppValue + "." + "input_dir")
      .asScala.toList
  }
  val outputottshowdir = config.getString(configAppValue + "." + "outputgumgumdir")
  val writetojson = Try(config.getBoolean(configAppValue + "." + "writetojson")).getOrElse(false)
  val writeToOrcHDFS = Try(config.getBoolean(configAppValue + "." + "writetoorchdfs")).getOrElse(false)
  val writetohive = Try(config.getBoolean(configAppValue + "." + "writetohive")).getOrElse(false)
  val hivetablename = Try(config.getString(configAppValue + "." + "hivetablename")).getOrElse("dummy.dummy")

  val docount = Try(config.getBoolean(configAppValue + "." + "count")).getOrElse(false)

  configLogger.info("Count is : " + config.getBoolean(configAppValue + "." + "count"))
  def readJsonFileIntoDF(inputDataDir:List[String]) : DataFrame = {
    spark.read
      .format("json")
      .load(inputDataDir:_*)
  }

  def readOrcFileIntoDF(inputDataDir:List[String]) : DataFrame = {
        spark.read
               .format("orc")
               .load(inputDataDir:_*)
    }
    
    def readOrcFileIntoDF(inputDataDir:String, selectCols:List[ColumnName]) : DataFrame = {
        spark.read
               .orc(inputDataDir)
               .select(selectCols: _*)
    }
    
    def readOrcFileIntoDF(inputDataDir:String, selectCols:List[ColumnName], limit:Integer) : DataFrame = {
        spark.read
               .orc(inputDataDir)
               .select(selectCols: _*)
               .limit(limit)
    }
    
    def readOrcFileIntoDF(inputDataDir:List[String], selectCols:List[ColumnName]) : DataFrame = {
        spark.read
               .format("orc")
               .load(inputDataDir:_*)
               .select(selectCols: _*)
    }
    
    def readOrcFileIntoDF(inputDataDir:List[String], selectCols:List[ColumnName], limit:Integer) : DataFrame = {
        spark.read
               .format("orc")
               .load(inputDataDir:_*)
               .select(selectCols: _*)
               .limit(limit)
    }
    
    def readHiveTableIntoDF(inputDataTable:String) : DataFrame = {
        spark.table(inputDataTable)
    }
    
    def readHiveTableIntoDF(inputDataTable:String, selectCols:List[ColumnName]) : DataFrame = {
        spark.table(inputDataTable)
               .select(selectCols: _*)
    }
    
    def readHiveTableIntoDF(inputDataTable:String, selectCols:List[ColumnName], limit:Integer) : DataFrame = {
        spark.table(inputDataTable)
               .select(selectCols: _*)
               .limit(limit)
    }


    def getFinalCols(df : DataFrame, removeCols: Set[String]) = {
      val colNames = df.columns.toSet
      val finalColNames = colNames -- removeCols
      import org.apache.spark.sql.functions._
  
      val finalDFCols = {
        finalColNames.toList.map(colName => {
            col(colName)
        } 
      )}
      finalDFCols
  }

  def getAllColNames (df:DataFrame*) = { 
    var allColNames = Set[String]()
    df.foreach(allColNames ++ _.columns.toSet)
    allColNames
  }
  
  def createCombinedColList(dfColNames: Set[String], allColNames: Set[String]) = {
    import org.apache.spark.sql.functions._
    allColNames.toList.map(x => x match {
      case x if dfColNames.contains(x) => col(x)
      case _ => lit("N/A").as(x)
    })
  }
  
  def writeDF(df: DataFrame, outputdir: String, datadt: String) = {
    import org.apache.spark.sql.functions._
    import spark.implicits._
 
    val finalDF = df.withColumn("data_dt", lit(datadt))
    //first create the table in hive if it doesnt already exist as an external table
    // and then write the orc file
    if (writetohive && writeToOrcHDFS) {
      val hiveDFSchemaFields = finalDF.schema.fields
      var schemaString = ""
      hiveDFSchemaFields.foreach(f =>
        if (!f.name.equalsIgnoreCase("data_dt"))
          if (f.dataType.typeName.equalsIgnoreCase("integer") || f.dataType.typeName.equalsIgnoreCase("long"))
          schemaString += f.name + " " + "int" + ","
        else if (f.dataType.typeName.equalsIgnoreCase("array"))
          schemaString += f.name + " " + "array<string>" + ","
        else
          schemaString += f.name + " " + f.dataType.typeName + ",")

      spark.sql("CREATE EXTERNAL TABLE IF NOT EXISTS " + hivetablename
        + " (" + schemaString.substring(0, schemaString.length - 1) + ")"
        + " STORED AS ORC "
        + " PARTITIONED BY (data_dt string) "
        + " LOCATION '" + outputdir + "'")
    }

    if (writeToOrcHDFS)
      finalDF.coalesce(1).write.format("csv").mode(SaveMode.Overwrite).partitionBy("data_dt").save(outputdir)

    if (writetojson) //only for dev testing
      finalDF.write.mode(SaveMode.Overwrite).json("/Users/admin/samplejson/")

    finalDF
  }
}