package com.att.datasolutions

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.logging.Level
import java.util.logging.Logger
import org.apache.spark.sql.functions._
import com.att.datasolutions.GumGumBase._
import org.apache.spark.sql.DataFrame

object GumgumDataProcess {
  import spark.implicits._
  val appLogger = Logger.getLogger(this.getClass.getName)
  appLogger.setLevel(Level.WARNING)
  
  def main(args: Array[String]): Unit = {
    val startpartdt:String = {
       if(args.length > 0) 
         args(0)
       else
         new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance.getTime)
     }

    appLogger.info("starting GumgumDataProcess")
    var now = System.nanoTime

    val initialDataProcess = loadDataProcess()

    var timeElapsed = (System.nanoTime - now) / scala.math.pow(10, 9)
    appLogger.info(s"Elapsed time: $timeElapsed secs")
  }

  def loadDataProcess() : DataFrame = {
    val columnNamesDF = {
      readJsonFileIntoDF(input_dir).toDF()
    }

    if (docount) columnNamesDF.show()
    import spark.implicits._
    import org.apache.spark.sql.expressions.Window

    val vistorIDDF = Window.partitionBy('visitorId).orderBy('timestamp.desc)
    val grpDF = columnNamesDF.withColumn("rank", dense_rank.over(vistorIDDF))
                             .withColumn("rank_2",$"rank"+1)
                             .withColumn("rank",'rank.cast("string"))
                             .select($"id",$"timestamp",$"type",$"visitorId",$"pageUrl",$"rank",$"rank_2")

    grpDF.createOrReplaceTempView("CTE")

    val dummDF=  spark.sql("select t1.id,t1.timestamp, t1.type, t1.visitorId, t1.pageUrl, t2.pageUrl as nextpageUrl from CTE t1  left outer join CTE t2 on t1.visitorId=t2.visitorId and t1.rank = t2.rank-1")
    val noNulDummDF = dummDF.filter("nextpageUrl is not null")

    if (docount) noNulDummDF.show()
    if (docount) noNulDummDF.printSchema()

    writeDF(noNulDummDF,outputottshowdir,"20180331")
  }

}//Closing