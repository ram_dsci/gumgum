package com.att.datasolutions

import org.apache.spark.sql.SparkSession

trait SparkSessionTestWrapper {
  lazy val spark: SparkSession = {
    SparkSession
      .builder
      .master("local[2]")
      .appName("Unit test")
      .getOrCreate()
  }
  spark.sparkContext.setLogLevel("WARN")
}